# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'Inouk Data Migration Toolkit Batch Loader',
    'version': '0.1',   # Eg. 0.1 : Warning used for migration scripts
    'author': 'Cyril MORISSE - ( @cmorisse )',
    'category': 'Inouk',
    'description': """
Addon that allows to asynchonouly load batches of CSV files.
Based on the openerpconnector, this module inherit all his job / queue machinery.
""",
    'website': '',
    'images': [],
    'depends': [
        'base_setup', 'connector',
    ],
    'data': [
        #'security/connector_security.xml',
        #'security/ir.model.access.csv',

        # sequences
        #'data/ir_sequence.xml',

        # backend default configution
        'data/idmt_backend_data.xml',

        # menus
        'menus/main_menu.xml',

        # views
        'views/backend_view.xml',
    ],
    'js': [],
    'qweb': [],
    'css': [],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
