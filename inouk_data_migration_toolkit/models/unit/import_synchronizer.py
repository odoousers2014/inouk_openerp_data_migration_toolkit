# -*- coding: utf-8 -*-
##############################################################################
#
#    Author: Cyril MORISSE - Audaxis ( @cmorisse )
#    Copyright 2014 Audaxis
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging
from openerp.tools import convert_xml_import
from openerp.tools.translate import _
import os
import csv
import os.path
from datetime import datetime
from openerp import SUPERUSER_ID
from openerp.loglevels import ustr
from openerp.addons.connector.queue.job import job
from openerp.addons.connector.connector import ConnectorUnit
from openerp.addons.connector.unit.synchronizer import ImportSynchronizer
from openerp.addons.connector.exception import InvalidDataError, ConnectorException
from ..backend import idmt_10csv
from ..connector import get_environment, add_checkpoint

_logger = logging.getLogger("InoukDataMigrationToolkit")

"""
Importers for Inouk Data Migration Toolkit.
"""

class InoukLoaderException(RuntimeError):
    """ Base Exception for the connectors """
    pass


from openerp.osv.orm import BaseModel, fix_import_export_id_paths

def load_without_savepoint(self, cr, uid, fields, data, context=None):
    """
    Attempts to load the data matrix, and returns a list of ids (or
    ``False`` if there was an error and no id could be generated) and a
    list of messages.

    The ids are those of the records created and saved (in database), in
    the same order they were extracted from the file. They can be passed
    directly to :meth:`~read`

    :param fields: list of fields to import, at the same index as the corresponding data
    :type fields: list(str)
    :param data: row-major matrix of data to import
    :type data: list(list(str))
    :param dict context:
    :returns: {ids: list(int)|False, messages: [Message]}
    """
    messages = []

    fields = map(fix_import_export_id_paths, fields)
    ModelData = self.pool['ir.model.data'].clear_caches()

    fg = self.fields_get(cr, uid, context=context)

    mode = 'init'
    current_module = ''
    noupdate = False

    ids = []
    for id, xid, record, info in self._convert_records(cr, uid,
                                                       self._extract_records(cr, uid, fields, data,
                                                                             context=context, log=messages.append),
                                                       context=context, log=messages.append):

        try:
            ids.append(ModelData._update(cr, uid, self._name,
                                         current_module, record, mode=mode, xml_id=xid,
                                         noupdate=noupdate, res_id=id, context=context))
        except Exception as e:
            return {'ids': ids, 'exception': e}

    return {'ids': ids, 'exception': None}

BaseModel.load_without_savepoint = load_without_savepoint

class IDMTImportSynchronizer(ImportSynchronizer):
    """
    Base importer for InoukDataMigrationToolkit
    """

    def __init__(self, environment):
        """
        :param environment: current environment (backend, session, ...)
        :type environment: :py:class:`connector.connector.Environment`
        """
        super(IDMTImportSynchronizer, self).__init__(environment)

    def _before_import(self):
        """
        Hook called at the very beginning of the import as soon as we have Reflex data
        """
        return

    def _after_import(self):
        """
        Hook called at the very end of the import
        """
        return

    def _import_file(self, file_name):
        """
        Import just one file
        """
        pass  # TODO: implement

    def run(self,  file_name):
        """
        import one file
        """
        self._after_import()

        return self._import_file(file_name)

        self._after_import()


@idmt_10csv
class IDMTBatchImportSynchronizer(IDMTImportSynchronizer):
    """
    IDMTImportSynchronizer
    """
    _model_name = 'idmt.backend'

    def load_csv_file_content(self, file_name, model_name, quote_char='"', delimiter_char=','):
        """
        Import csv file with:
            - quote: "
            - delimiter: ,
            - encoding: utf-8
        """

        # read file and setup csv reader
        csv_file = open(os.path.join(self.backend_record.source_files_directory, file_name))
        csv_reader = csv.reader(csv_file, quotechar=quote_char, delimiter=delimiter_char)

        # retreive fields list
        fields = csv_reader.next()

        uid = SUPERUSER_ID
        datas = []
        for line in csv_reader:
            # skip empty lines
            if (not line) or not reduce(lambda x, y: x or y, line):
                continue
            try:
                datas.append(map(lambda x: ustr(x), line))
            except:
                _logger.error("Cannot import the line: %s", line)

        model_obj = self.session.pool.get(model_name)
        return model_obj.load_without_savepoint(self.session.cr, uid, fields, datas, context=self.session.context)


    def import_csv_file(self, file_name, model_name):
        """
        For CSV, we must browse file content to extract data then use osv.load() method
        to process extracted data.
        """
        result = self.load_csv_file_content(file_name, model_name)
        if result['exception'] is None and self.backend_record.no_update:
            ir_model_data = self.session.pool.get('ir.model.data')
            imd_ids = ir_model_data.search(self.session.cr, self.session.uid, [('res_id', 'in', result['ids'])])
            ir_model_data.write(self.session.cr, self.session.uid, imd_ids, dict(noupdate=True))

        return result



    def import_xml_file(self, file_name, module_name):
        """
        Things are easier for xml since we can call odoo method straight.
        """
        xml_file_path = open(os.path.join(self.backend_record.source_files_directory, file_name))
        try:
            result = convert_xml_import(self.session.cr, module_name, xml_file_path)
            if result:
                return {'ids': None, 'exception': None}
        except Exception as e:
            return {'ids': None, 'exception': e}




    def run(self, files_text_list):
        """
        Entry point to process an import file list
        """
        uid = SUPERUSER_ID
        files_text_list = files_text_list.replace('\n', '|')
        file_names_list = filter(None, files_text_list.split('|'))

        self._before_import()

        job_result = ''
        file_type = None
        module_name = None

        for file_name in file_names_list:
            # TODO: import XML or CSV depending on file ext
            # Extract model name and check that model exists on server
            file_type = file_name.split('.')[-1].lower()

            if file_type == 'xml':
                module_name = file_name.split('.')[0]
                model_name = None  # for XML model is define per loaded object
                if not module_name:
                    raise InvalidDataError(_("For xml files you must define a module in the file name. "
                                             "Use a name of the form module_name.whatever_you_want.xml."))

            elif file_type == 'csv':
                module_name = None  # For CSV module is defined in file on a per line basis

                model_name = file_name[:-4]
                model_name = model_name.split('-')[0]  # remove file differentiator

                try:
                    model_obj = self.session.pool.get(model_name)
                except Exception:
                    raise InvalidDataError(_("'%s' is not a valid model name. Unable to process file '%s' !" % (model_name, file_name,)))

            else:
                raise InvalidDataError(_("'%s' is not a supported file type !."))

            file_import_result = ''

            message = "File '%s' import into object '%s' started" % (file_name, model_name,)
            _logger.info(message)
            file_import_result += "%s\n" % message

            begin_time = datetime.now()
            if file_type == 'xml':
                result = self.import_xml_file(file_name, module_name)

            elif file_type == 'csv':
                result = self.import_csv_file(file_name, model_name)

            else:
                # Should never pass here
                raise ConnectorException(_("Inouk Data Loader Internal Error"))

            _logger.info("File '%s' result = %s", file_name, result)

            end_time = datetime.now()
            duration = end_time - begin_time

            if result['exception']:
                message = "File '%s': Exception during import" % (file_name,)
                _logger.info(message)
                file_import_result += "%s\n" % message

                job_result += file_import_result
                job_result += "----------------( Encoutered exception )----------------\n"
                job_result += "Exception: %s" % result['exception'].__class__.__name__ + "\n"
                job_result += result['exception'].__str__()
                error_message = "\n----------------( Log before exception )----------------\n" + job_result
                raise InoukLoaderException(error_message)


            else:  # success bit with XML we don't have ids
                if result['ids']:
                    message = "File '%s': %s records processed in %s seconds" % (file_name, len(result['ids']), duration,)
                else:
                    message = "File '%s': processed in %s seconds" % (file_name, duration,)
                _logger.info(message)
                file_import_result += "%s\n" % message


            if self.backend_record.commit_after_each_file:
                message = "File '%s': commit() called after loading." % (file_name,)
                _logger.info(message)
                file_import_result += "%s\n" % message
                self.session.cr.commit()

            job_result += file_import_result


        self._after_import()
        return job_result


@job
def csv_file_import_batch(session, backend_id, import_files_list):
    """Sequentially Import a set of files."""

    env = get_environment(session, 'idmt.backend', backend_id)
    csv_importer = env.get_connector_unit(IDMTBatchImportSynchronizer)
    job_result = csv_importer.run(import_files_list)
    return job_result


@idmt_10csv
class AddCheckpoint(ConnectorUnit):
    """ Add a connector.checkpoint on the underlying model
    (not the magento.* but the _inherits'ed model) """
    _model_name = ['magento.product.product',
                   'magento.product.category',
                   'magento.store',
                   ]

    def run(self, openerp_binding_id):
        binding = self.session.browse(self.model._name, openerp_binding_id)
        record = binding.openerp_id
        add_checkpoint(self.session,
                       record._model._name,
                       record.id,
                       self.backend_record.id)


# DELETE FROM product_template;
# DELETE FROM product_product;
# DELETE FROM magento_product_category;
# DELETE FROM product_category where id > 5;
# DELETE FROM ir_model_data where module = 'dmi';
#
# SELECT * FROM product_category order by id;
# SELECT * FROM ir_model_data where module = 'dmi';
#
# SELECT * FROM queue_job order by id;
