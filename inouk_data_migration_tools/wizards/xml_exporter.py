# -*- encoding: utf-8 -*-
##############################################################################
#
#    Inouk Data Migration Toolkit
#    Copyright (C) 2013-2015 Cyril MORISSE (twitter: @cmorisse)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import base64
import time

from openerp.osv import osv, fields
from openerp import tools
from openerp.tools.safe_eval import safe_eval
from openerp.tools.translate import _
import ast

from openerp import SUPERUSER_ID
from openerp.osv import expression
from xml.dom import minidom

import logging

_logger = logging.getLogger("XmlExporter")


WIZARD_TITLE = "Export objects as XML"

XML_EXPORT_STATES = [
    ('1', "Step 1: Define what to export"),
    ('2', "Step 2: Define export parameters"),
    ('3', "Step 3: Review selected objects"),
    ('4', "Step 4: Download XML file"),
]


class XmlExporter(osv.osv_memory):
    _name = 'idmt.xml_exporter'
    _description = "Xml Exporter Wizard object"

    _columns = {
        'state': fields.selection(XML_EXPORT_STATES, _("State")),
        'object_id': fields.many2one('ir.model', "Exported object"),
        'export_template_id': fields.many2one('ir.exports', "CSV Template", help="Exported fields are defined by the CSV Template you select."),

        'filter_date_condition': fields.selection([('created', 'Created'), ('modified', 'Modified'), ('created_modified', 'Created or Modified')], "Exports only"),
        'filter_date': fields.datetime('Export from Date'),
        'export_domain': fields.text("Export domain", help="Odoo domain to filter exported data. Eg. [('age', '=', 30)]. Note that if a domain is defined, date criterion above are ignored."),

        'module_name': fields.char("Default module", size=64,
                                   help="For exported objects with several external ids, select the one with this "
                                        "module if it exists. For exported objects with no external ids, create one "
                                        "with this module."),
        'strict_mode': fields.boolean("Strict mode",
                                      help="Raise an error, if try to export an object that reference that has not been "
                                           "exported or imported."),
        'no_update': fields.boolean("No update",
                                    help="Check if generated should never be updated but only created (eg. for invoice sequence)."),

        'preferred_module_name': fields.char("Preferred module name", size=64,
                                             help="In case, several External IDs points to the objects, here you can "
                                                  "define a module name that will be used to select the external id"
                                                  "that will be exported."),
        'external_id_generator_expression': fields.char("External id format function",
                                                        help="Optional python to define generated external id names eg. "
                                                        "'pillow_%s_%s' % (o._model._table, o.id) where o is a browse"
                                                        " of the currently exported object."),

        'output_mode': fields.selection([('string', "a string to copy and paste in your code"), ('file', "an XML File")],
                                        string="Exports as", required=True),
        'xml_file_name': fields.char("XML file name", size=256,
                                     help="Optional file name you want to use."),
        'xml_file_data': fields.binary("XML File"),
        'xml_result': fields.text("XML Result"),


        # Step 3: Review selected objects
        'to_export_ids': fields.text("Selected ids"),
    }

    _defaults = {
        'state': '1',
        'filter_date': None,
        'filter_date_condition': None,
        'object_id': 61,
        'export_template_id': 18,
        'export_domain': '',
        'to_export_ids': '',
        'module_name': '',
        'strict_mode': False,
        'no_update': True,
        'external_id_generator_expression': '',
        'output_mode': 'string',
        'xml_file_data': lambda o, cr, uid, context: base64.encodestring("CoinCoin"),
    }

    # WARNING
    # In onchange ids is always [] as the object does not exist yet.
    # So we must pass all fields required by computation to the onchange method
    #
    def onchange_checkdate(self, cr, uid, ids, filter_date, filter_date_condition, context=None):
        if not context:
            context = {}
        export_domain = ''
        if filter_date:
            if filter_date_condition == 'created':
                export_domain = "[('create_date', '>', '%s')]" % filter_date
            elif filter_date_condition == 'modified':
                export_domain = "[('write_date', '>', '%s')]" % filter_date
            elif filter_date_condition == 'created_modified':
                export_domain ="['|',('create_date', '>', '%s'), ('write_date', '>', '%s')]" % (filter_date, filter_date,)

        return {
            'value': {
                'export_domain': export_domain
            }
        }


    def do_validate_selection(self, cr, uid, ids, context=None):
        """
        """
        ir_model_data_obj = self.pool.get('ir.model.data')
        wizard_brws = self.browse(cr, uid, ids[0], context=context)

        # TODO: validate that entered values product a valid selection
        # If false stay on the same object by returning an error
        # If true return result view / object

        to_export_ids = self.filter_objects_to_export(cr, uid, wizard_brws.object_id.model,
                                                     wizard_brws.export_domain, context=context)
        if not to_export_ids:
            raise osv.except_osv("Selection problem", "Entered criterion return no records. Correct criterion or cancel!" )

        result = self.write(cr, uid, [wizard_brws.id],
                            {'state': '2', 'to_export_ids': to_export_ids},
                            context=context),

        # We have some ids, let's goto step 2
        _logger.debug("Goto step 2 result = %s with ids='%s'", result, to_export_ids)

        # Return same wizard but on step 2
        view_rec = ir_model_data_obj.get_object_reference(cr, uid, 'inouk_data_migration_tools', 'xml_exporter_wizard_view')
        view_id = view_rec and view_rec[1] or False

        # Pour définir une valeur dans le wizard qui va s'afficher
        context['default_export_domain'] = (wizard_brws.export_domain and wizard_brws.export_domain + "again... ") \
                                           or "again... "

        return {
            'name': WIZARD_TITLE,
            'view_type': 'form',
            'view_id' : [view_id],
            'view_mode': 'form',
            'res_model': 'idmt.xml_exporter',
            'type': 'ir.actions.act_window',
            'res_id': wizard_brws.id,  # On ajoutant cette ligne, on rouvre le même
            'target': 'new',           # objet.
            'context': context
        }

    def do_validate_export_parameters(self, cr, uid, ids, context=None):
        """
        Validate export params and goto step 3
        """
        ir_model_data_obj = self.pool.get('ir.model.data')
        wizard_brws = self.browse(cr, uid, ids[0], context=context)

        result = self.write(cr, uid, [wizard_brws.id],
                            {'state': '3'}, context=context),

        # Return same wizard but on step 3
        view_rec = ir_model_data_obj.get_object_reference(cr, uid, 'inouk_data_migration_tools', 'xml_exporter_wizard_view')
        view_id = view_rec and view_rec[1] or False

        return {
            'name': "Export objects as XML",
            'view_type': 'form',
            'view_id' : [view_id],
            'view_mode': 'form',
            'res_model': 'idmt.xml_exporter',
            'type': 'ir.actions.act_window',
            'res_id': wizard_brws.id,  # On ajoutant cette ligne, on rouvre le même
            'target': 'new',           # objet.
            'context': context
        }


    def go_back_to_previous_step(self, cr, uid, ids, context=None):
        """
        Bind to back button on Step 3
        """
        _logger.debug("go_back_to_previous_step()")

        ir_model_data_obj = self.pool.get('ir.model.data')
        wizard_brws = self.browse(cr, uid, ids[0], context=context)

        # Goto previous step
        # Cant be 1 as the button is not displayed on form if state is 1
        previous_state = str(int(wizard_brws.state) - 1)

        self.write(cr, uid, wizard_brws.id, {'state': previous_state}, context=context)

        # Retreive view id of wizard form view
        view_rec = ir_model_data_obj.get_object_reference(cr, uid, 'inouk_data_migration_tools', 'xml_exporter_wizard_view')
        view_id = view_rec and view_rec[1] or False

        return {
            'name': WIZARD_TITLE,
            'view_type': 'form',
            'view_id' : [view_id],
            'view_mode': 'form',
            'res_model': 'idmt.xml_exporter',
            'type': 'ir.actions.act_window',
            'res_id': wizard_brws.id,  # On ajoutant cette ligne, on rouvre le même
            'target': 'new',           # objet.
            'context': context
        }


    def do_export_objects(self, cr, uid, ids, context=None):
        """
        Call by Step 2: button do launch export
        :param ids: id of wizard
        :type ids: [int]
        :param context:
        :type context:
        :return:
        :rtype:
        """
        ir_model_data_obj = self.pool.get('ir.model.data')
        wizard_brws = self.browse(cr, uid, ids[0], context=context)

        # TODO: populate res with real content
        xml_result_as_text = self.export_objects(cr, uid, ids[0], context=context)


        # Goto Step 4
        if wizard_brws.output_mode == 'string':
            step4_update_values = {
                'xml_result': xml_result_as_text,
                'state': '4',
            }

        else:
            step4_update_values = {
                'xml_file_data': base64.encodestring(xml_result_as_text),
                'state': '4',
            }

        self.write(cr, uid, wizard_brws.id, step4_update_values, context=context)

        # Retreive view id of wizard form view
        view_rec = ir_model_data_obj.get_object_reference(cr, uid, 'inouk_data_migration_tools', 'xml_exporter_wizard_view')
        view_id = view_rec and view_rec[1] or False

        target_action = {
            'name': WIZARD_TITLE,
            'view_type': 'form',
            'view_id' : [view_id],
            'view_mode': 'form',
            'res_model': 'idmt.xml_exporter',
            'type': 'ir.actions.act_window',
            'res_id': wizard_brws.id,  # On ajoutant cette ligne, on rouvre le même
            'target': 'new',           # objet.
            'context': context
        }
        return target_action




    #
    # This code will move to the export job
    #

    def _eval_context(self, cr, uid):
        """Returns a dictionary to use as evaluation context for
           search domains."""
        return {
            'user': self.pool.get('res.users').browse(cr, SUPERUSER_ID, uid),
            'time': time,
        }

    def _eval_domain(self, cr, uid, domain, context=None):
        return expression.normalize_domain(eval(domain, self._eval_context(cr, uid)))


    def filter_objects_to_export(self, cr, uid, model_name, domain, context=None):
        """
        :param model_name: a valid ir_model name
        :type model_name: str
        :param domain:
        :type domain: str
        """
        if not domain:
            domain = "[]"
        exported_obj = self.pool.get(model_name)

        parsed_domain = self._eval_domain(cr, uid, domain)
        exported_ids = exported_obj.search(cr, uid, parsed_domain, context=context)

        return exported_ids

    def normalize_fields(self, cr, uid, template_id, context=None):
        """
        :param template_id: A valid ir_export id
        :type template_id: int
        """
        ir_export_obj = self.pool.get('ir.exports')
        ir_export_brws = ir_export_obj.browse(cr, uid, template_id, context=context)
        raw_field_list = [ f.name for f in ir_export_brws.export_fields]
        normalized_field_list = map(lambda e: e.split('/')[0], raw_field_list)
        return normalized_field_list

    def lookup_or_create_external_id(self, cr, uid, model, id, create=False, module_name=None, name_generator_expression=None, context=None):

        if not context:
            context={}
        if not name_generator_expression:
            name_generator_expression = '"%s__%s" % (o._model._table, o.id,)'
        if isinstance(id, (tuple, list,)):
            id = id[0]

        ir_model_data_obj = self.pool.get('ir.model.data')
        search_domain = [('model', '=', model), ('res_id', '=', id)]
        external_ids = ir_model_data_obj.search(cr, uid, search_domain)
        if not external_ids:
            if create:
                object_brws = self.pool.get(model).browse(cr, uid, id, context=context)
                external_id = safe_eval(name_generator_expression, {}, dict(o=object_brws))
                result = ir_model_data_obj.create(cr, uid,
                                                  {
                                                      'name': external_id,
                                                      'model': model,
                                                      'module': module_name or '__export__',
                                                      'res_id': id,
                                                  }, context=context)
                if result:
                    return "%s.%s" % (module_name, external_id,)

            return None

        if len(external_ids) > 1 and module_name:
            # TODO search among ids the one with moodule
            external_ids = [external_ids[0]]

        obj = ir_model_data_obj.browse(cr, uid, external_ids[0])

        result = obj.module+'.'+obj.name
        _logger.debug("lookup_external_id => %s", result)
        return result


    def export_object(self, cr, uid, xml_doc, model_obj, id, field_list, strict_mode=False, module_name=None, context=None):
        """
        """
        obj_dict = model_obj.read(cr, uid, id, field_list, context)

        xml_record = xml_doc.createElement('record')
        external_id = self.lookup_or_create_external_id(cr, uid, model_obj._name, id, create=True, module_name=module_name)
        if external_id:
            xml_record.setAttribute("id", str(external_id))  # TODO: fetch external id
        else:
            raise osv.except_osv("Internal ERROR", "Unable to generate External Id for object '%s':%s" % (model_obj._name, id,))

        xml_record.setAttribute("model", model_obj._name)

        # special process for field id
        del obj_dict['id']

        for key, value in obj_dict.items():
            field_definition = model_obj._all_columns[key].column
            field_type = field_definition._type

            # if no value we skip generation
            if not value and field_type != 'boolean':
                continue

            if field_type in ('integer', 'float') or (field_type=='selection' and isinstance(value, int)):
                xml_field = xml_doc.createElement('field')
                xml_field.setAttribute("name", key)
                xml_field.setAttribute("eval", value and str(value) or 'False' )
                xml_record.appendChild(xml_field)

            elif field_type == 'boolean':
                xml_field = xml_doc.createElement('field')
                xml_field.setAttribute("name", key)
                xml_field.setAttribute("eval", value and '1' or '0' )
                xml_record.appendChild(xml_field)

            elif field_type == 'one2many':
                _logger.debug("one2many are not exported.")
                continue

            elif field_type == 'many2one':
                xml_field = xml_doc.createElement('field')
                xml_field.setAttribute("name", key)

                target_model_name = field_definition._obj

                # get external id
                #id = self.lookup_external_id(cr, uid, target_model_name, value, default_module='test')
                external_id = self.lookup_or_create_external_id(cr, uid, target_model_name, value)

                if not external_id:
                    if strict_mode:
                        raise osv.except_osv("Strict mode violation",
                                             "Trying to export a reference to object '%s', id=%s that has "
                                             "no external id !!!" % (target_model_name, value,))
                    else:  # We insert a search on target object _rec_name
                        target_obj = self.pool.get(target_model_name)
                        target_id = value[0]  # eg. (76, 'France')

                        xml_field.setAttribute("model", target_model_name)
                        field_name = target_obj._rec_name
                        target_object_name = target_obj.read(cr, uid, target_id, [field_name])[field_name] or False
                        xml_field.setAttribute("search", str([(str(field_name) ,'=', target_object_name)]))
                else:
                    xml_field.setAttribute("ref", external_id)
                xml_record.appendChild(xml_field)

            elif field_type == 'many2many':
                #
                # We must generate field content of form:
                #    [(6, 0, [1, 2, 4])]
                # Where 6 is "set field content with"  and 1,2,4 the value itself

                xml_field = xml_doc.createElement('field')
                xml_field.setAttribute("name", key)
                target_model_name = field_definition._obj

                res = []
                for target_object_id in value:
                    external_id = self.lookup_or_create_external_id(cr, uid, target_model_name, target_object_id, module_name=module_name)

                    if not external_id:  # We insert a search # TODO: Add a param to decide id
                        if strict_mode:
                            raise osv.except_osv("Strict mode violation",
                                                 "Trying to export a reference to object '%s', id=%s that has "
                                                 "no external id !!!" % (target_model_name, target_object_id,))
                        res.append(target_object_id)
                    else:
                        res.append(external_id)

                xml_field.setAttribute("eval", "[(6,0,["+','.join(map(lambda x: ("ref('%s')" if isinstance(x, unicode) else "%s") % (x,), res))+'])]')

                xml_record.appendChild(xml_field)

                _logger.debug("res => %s" % res)
                _logger.debug("eval=\"[(6,0,["+','.join(map(lambda x: ("ref('%s')" if isinstance(x, unicode) else "%s") % (x,), res))+'])]')

            else:
                xml_field = xml_doc.createElement('field')
                xml_field.setAttribute("name", key)
                xml_field.appendChild(xml_doc.createTextNode(value))
                xml_record.appendChild(xml_field)

        _logger.debug("generated_object = %s", xml_record.toprettyxml(indent="  ").encode('utf-8'))
        return xml_record

    def export_objects(self, cr, uid, wizard_id, context=None):
        """
        :param wizard_id: a valid wizard id
        :type wizard_id: int
        """
        if not context:
            context={}

        wizard_brws = self.browse(cr, uid, wizard_id, context=context)
        model_to_export = wizard_brws.object_id.model
        to_export_ids = self.filter_objects_to_export(cr, uid, model_to_export,
                                                      wizard_brws.export_domain,
                                                      context=context)

        # retrieve object fields
        fields_to_export = self.normalize_fields(cr, uid, wizard_brws.export_template_id.id, context=context)
        exported_obj = self.pool.get(wizard_brws.object_id.model)

        xml_doc = minidom.Document()
        xml_openerp = xml_doc.createElement("openerp")
        xml_doc.appendChild(xml_openerp)

        xml_data = xml_doc.createElement("data")
        if wizard_brws.no_update:
            xml_data.setAttribute("noupdate", "1")
        xml_openerp.appendChild(xml_data)

        for id in to_export_ids:
            a_record = self.export_object(cr, uid, xml_doc, exported_obj, id, fields_to_export,
                                          module_name=wizard_brws.module_name,
                                          strict_mode=wizard_brws.strict_mode,
                                          context=context)
            xml_data.appendChild(a_record)

        return xml_doc.toprettyxml(indent="    ").encode('utf-8')




class XmlExporterGeneratedData(osv.osv_memory):
    _name = 'idmt.xml_exporter_generated_data'
    _description = "Xml Exporter Generated Data Wizard"

    _columns = {
        'xml_result': fields.text('Result'),
    }


#vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
