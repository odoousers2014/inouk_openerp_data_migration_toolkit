# -*- coding: utf-8 -*-
##############################################################################
#
#    Inouk Data Migration Toolkit
#    Copyright (C) 2013-2015 Cyril MORISSE (twitter: @cmorisse)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Inouk Data Migration Tools',
    'version': '1.0',
    'category': 'Inouk',
    'description': """
This module allows to import and edit CSV Import Export Templates and export object in XML using the CSV templates.




""",
    'author': 'Cyril MORISSE - @cmorisse',
    #'website': '',
    'depends': ['base'],
    'data': [
        'views/menu.xml',
        'views/ir_exports.xml',

        'wizards/xml_exporter_view.xml',
    ],
    'demo_xml': [],
    'installable': True,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
