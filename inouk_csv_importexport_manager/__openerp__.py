# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

# TODO: Add a system to generate window action for objects unreachable in odoo standard

{
    'name': 'Inouk CSV Import Export Manager',
    'version': '1.0',
    'category': 'Tools',
    'description': """
This module allows you to import and edit CSV Import Export Templates.
These templates can be selected on the export windows.
""",
    'author': 'Cyril MORISSE - @cmorisse',
    'website': 'http://blog.inouk.fr',
    'depends': ['base'],
    'data': [
        'views/menu.xml',
        'views/ir_exports.xml',
    ],
    'demo_xml': [],
    'installable': True,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
